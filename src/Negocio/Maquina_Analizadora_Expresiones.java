package Negocio;


import util.colecciones_seed.Pila;

/*package Negocio;

import util.colecciones_seed.Pila;

/**
 *
 * @author docenteauditorio
 */
public class Maquina_Analizadora_Expresiones {

    public Maquina_Analizadora_Expresiones() {
    }

    /**
     *
     * permite analizar si una expresión contiene paréntesis balenceados
     *
     * @param cadena una expresión para analizar
     * @return true si la cadena contiene una expresión con paréntesis
     * balanceados o false en caso contrario
     */
    public boolean tieneParatensisBalanceados(String cadena) {
        Pila<Character> p = new Pila();
        char v[] = cadena.toCharArray();
        for (char dato : v) {
            if (dato == '(') {
                p.push(dato);
            } else {
                if (dato == ')') {
                    if (!p.esVacia()) {
                        p.pop();
                    } else {
                        return false;
                    }
                }
            }
        }
        return p.esVacia();
    }

    /**
     * Método evalúa una expresión del tipo L=<a*b*>
     * donde se cumple que: 2a=b Ejemplo1: aab --> true Ejemplo1: aabb --> false
     * Ejemplo1: abaaba --> true Ejemplo1: abaabacde --> false--> NO CADENA L
     *
     * @param cadena
     * @return true si la cadena contiene el doble de a(s) que de b(s)
     */
    public boolean tieneDobleCantidad_A(String cadena) {

        // Se crean las pilas A y B
        Pila<Character> a = new Pila();
        Pila<Character> b = new Pila();

        // Se crea el array donde se guarda los caracteres de la cadena
        char z[] = cadena.toCharArray();

        // se recorre el vector
        for (char dato : z) {
            System.out.println(dato);

            //se da la condicion si encuentra una a , la agrega a su respectiva pila
            if (dato == 'a') {
                a.push(dato);

                //se da la condicion si encuentra una b , la agrega a su respectiva pila
            } else if (dato == 'b') {
                b.push(dato);

                // si encuentra una letra distinta de a o b returna false.
            } else {

                return false;
            }

        }
        //se compara el tamaño de las pilas para validar si por cada b hay dos a.
        return (a.getSize() == 2 * b.getSize());

        /*
            NO SE PERMITE USAR CONTADORES, SOLO PILAS Y/O COLAS
         */
    }

    /**
     * Orden: 1. [] 2-{} 3. ()
     *
     * Ejemplo1: [.....()]--> true Ejemplo2: [.....({})]--> false
     *
     * ayuda: LIBRO DE CESAR BECERRA DE ESTRUCTURAS DE DATOS EN C++ O JAVA
     *
     * @param cadena
     * @return
     */

   

    public boolean tieneSignosAgrupacionBalanceados(String cadena) {

         // Se crea la pila p
        
        Pila<Character> p = new Pila();

        // Se crea el array donde se guarda los caracteres de la cadena
        char x[] = cadena.toCharArray();
        
        //se crean las variables de jerarquia para saber que se esta desapilando
         int jerarquia = 3;
         int jerarquiaA = 3;
         
         //se recorre el vector
        for (char dato : x) {
            
            //Se asigna jerarquia para reiniciar 
            jerarquiaA = jerarquia;
            
            //se da la condicion si encuentra una a , la agrega a su respectiva pila
            if (dato == '[' || dato == '{'|| dato == '('  ) {
                p.push(dato);
                
                //se da la condicion del funcionamiento de la jerarquia y returna false
                //cuando hay algo mayor que el dato siguiente. ejemplo 2>3 false.
                if(jerarquia<jerarquia(dato)){
                    
                    return false;
                }
                // retorna la jerarquia actual 
                jerarquia =jerarquia(dato);
                
               // 
            }
            if(dato == ']' || dato == '}'|| dato == ')'  ) {
                char c = p.pop();
                

                if(!verificador(c,dato))
                    return false;
                
                jerarquia = jerarquiaA;
            }
                     
            
        }

      return p.esVacia();   
    }
   
    public int jerarquia(char string){
     if(string == '[' ){
        return 3;     
     }
     if(string == '{'){
         return 2;     
     }
     return 1;
    }
    
    private boolean verificador(char c, char dato){
    
    switch(c){
        case '[':
            return dato == ']';
        case '(':
            return dato == ')';
        case '{':
            return dato == '}';
    }
    return false;
    }
}


