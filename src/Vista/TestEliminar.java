/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import util.colecciones_seed.ListaS;

/**
 *
 * @author Auditorio
 */
public class TestEliminar {

    public static void main(String[] args) {
        ListaS<Integer> l = new ListaS();
        l.insertarInicio(3);
        l.insertarInicio(5);
        l.insertarInicio(7);
        l.insertarInicio(9);
        System.out.println("Lista Original:"+l.toString());
        System.out.println("Borre:"+l.eliminar(2));
        System.out.println("La lista quedó:"+l.toString());
        System.out.println("Borre:"+l.eliminar(0));
        System.out.println("La lista quedó:"+l.toString());
    }

}
