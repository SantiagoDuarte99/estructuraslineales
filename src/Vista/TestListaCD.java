/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import util.colecciones_seed.ListaCD;

/**
 *
 * @author docenteauditorio
 */
public class TestListaCD {

    public static void main(String[] args) {
        ListaCD<String> l = new ListaCD();
        l.insertarInicio("Heydi");
        l.insertarInicio("Maiken");
        l.insertarInicio("Javier");
        l.insertarInicio("Daniela");
        l.insertarInicio("Saray");
        System.out.println(l.toString());
        
        //ESTO ES LA FORMA CANDIDA :(
        for(int i=0;i<l.getSize();i++)
            System.out.println("Elemento -"+i+"\t:"+l.get(i));

    }
}
